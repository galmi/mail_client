package client

import (
	"fmt"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"golang.org/x/net/proxy"
	"log"
	"net"
	"net/url"
	"time"
)

type MailClient struct {
	Client           *client.Client
	Encryption       string //plain, ssl, tls
	IncomingProtocol string //imap, pop3
	IncomingServer   string
	IncomingPort     int
	OutgoingServer   string
	OutgoingPort     int
	Login            string
	Password         string
	ProxyUrl         string
}

func (c *MailClient) createProxyDialer() (dialer client.Dialer, err error) {
	uri, err := url.Parse(c.ProxyUrl)
	if err != nil {
		fmt.Printf("FromURL failed: %v\n", err)
		return nil, fmt.Errorf("proxy_parse_error: %s", err)
	}
	if len(c.ProxyUrl) > 0 {
		dialer, err = proxy.FromURL(uri, proxy.Direct)
	} else {
		dialer = &net.Dialer{Timeout: time.Second * 10}
	}
	if err != nil {
		fmt.Printf("FromURL failed: %v\n", err)
		return nil, fmt.Errorf("proxy_parse_error: %s", err)
	}
	return
}

func (c *MailClient) Connect() (err error) {
	// Connect to server
	d, err := c.createProxyDialer()
	if err != nil {
		return err
	}
	serverUrl := fmt.Sprintf("%s:%d", c.IncomingServer, c.IncomingPort)
	if c.Encryption == "tls" {
		c.Client, err = client.DialWithDialerTLS(d, serverUrl, nil)
	} else {
		c.Client, err = client.DialWithDialer(d, serverUrl)
	}
	if err != nil {
		return err
	}
	log.Println("Connected")

	// Don't forget to logout
	//defer c.Logout()

	// Login
	if err := c.Client.Login(c.Login, c.Password); err != nil {
		return err
	}
	log.Println("Logged in")
	return nil
}

func (c *MailClient) Disconnect() (err error) {
	return c.Client.Logout()
}

func (c *MailClient) GetImapFolders() []string {
	var ref string
	var folders []string
	infoChan := make(chan *imap.MailboxInfo, 10)
	_ = c.Client.List(ref, "*", infoChan)
	for folder := range infoChan {
		folders = append(folders, folder.Name)
	}
	return folders
}

func (c *MailClient) GetFolderMessagesCount(folder string) uint32 {
	mbox, err := c.Client.Select(folder, true)
	if err != nil {
		log.Fatal(err)
	}
	return mbox.Messages
}

func (c *MailClient) GetFolderEmails(folder string, from uint32, count uint32) []*imap.Message {
	mbox, err := c.Client.Select(folder, true)
	if err != nil {
		log.Fatal(err)
	}

	// Get the last 4 messages
	from = mbox.Messages - from
	to := from - count + 1
	if to < 0 {
		to = 0
	}
	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)
	items := []imap.FetchItem{imap.FetchEnvelope, imap.FetchFlags, imap.FetchUid}

	messages := make(chan *imap.Message, 10)
	done := make(chan error, 1)
	go func() {
		done <- c.Client.Fetch(seqset, items, messages)
	}()

	msgs := []*imap.Message{}
	for msg := range messages {
		msgs = append(msgs, msg)
		//log.Println("* " + msg.Envelope.MessageId + " " + msg.Envelope.Subject)
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}

	return msgs
}

func (c *MailClient) GetEmail(messageSeqNum uint32) *imap.Message {
	//_, err := c.Client.Select(folder, true)
	//if err != nil {
	//	log.Fatal(err)
	//}

	seqset := new(imap.SeqSet)
	seqset.AddNum(messageSeqNum)
	items := []imap.FetchItem{imap.FetchFlags, imap.FetchInternalDate, imap.FetchRFC822Size, imap.FetchEnvelope, imap.FetchBody}
	messages := make(chan *imap.Message, 10)
	done := make(chan error, 1)
	go func() {
		done <- c.Client.Fetch(seqset, items, messages)
	}()

	for msg := range messages {
		return msg
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}

	return nil
}

func (c *MailClient) MarkAsSeen(folder string, messageSeqNum uint32) {
	_, err := c.Client.Select(folder, false)
	if err != nil {
		log.Fatal(err)
	}

	seqset := new(imap.SeqSet)
	seqset.AddNum(messageSeqNum)
	done := make(chan error, 1)
	go func() {
		done <- c.Client.Store(seqset, imap.AddFlags, []interface{}{imap.SeenFlag}, nil)
	}()

	if err := <-done; err != nil {
		log.Fatalf("c.Store() = %v\n", err)
	}
}

func (c *MailClient) MarkAsDeleted(messageSeqNum uint32) {
	_, err := c.Client.Select("INBOX", false)
	if err != nil {
		log.Fatal(err)
	}

	seqset := new(imap.SeqSet)
	seqset.AddNum(messageSeqNum)
	done := make(chan error, 1)
	go func() {
		done <- c.Client.Store(seqset, imap.AddFlags, []interface{}{imap.DeletedFlag}, nil)
	}()

	if err := <-done; err != nil {
		log.Fatalf("c.Store() = %v\n", err)
	}
}

func (c *MailClient) DeleteAll() {
	_, err := c.Client.Select("INBOX", false)
	if err != nil {
		log.Fatal(err)
	}
	if err := c.Client.Expunge(nil); err != nil {
		log.Fatal(err)
	}
}
