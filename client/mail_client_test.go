package client

import (
	"fmt"
	"testing"
)

func TestConnectImap(t *testing.T) {
	client := MailClient{
		Encryption:       "tls",
		IncomingServer:   "imap.yandex.com",
		IncomingPort:     993,
		IncomingProtocol: "imap",
		Login:            "***",
		Password:         "***",
		ProxyUrl:         "",
	}
	err := client.Connect()
	if err != nil {
		t.Fatal(err)
	}

	msgCount := client.GetFolderMessagesCount("INBOX")
	fmt.Println("Messages count", msgCount)

	msgs := client.GetFolderEmails("INBOX", 3, 2)
	for _, msg := range msgs {
		fmt.Printf("* %d %+v %s\n", msg.SeqNum, msg.Flags, msg.Envelope.Subject)
	}

	client.GetEmail(613)
	client.MarkAsSeen("INBOX", 613)
	client.MarkAsDeleted(613)
	client.DeleteAll()
}
